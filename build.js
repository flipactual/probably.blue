/* use strict */

const { stat, mkdir, readFile, writeFile } = require("fs").promises;
const moment = require("moment");
const { createCanvas } = require("canvas");

const FAVICON_DIMENSIONS = [
  [16, 16],
  [32, 32],
  [96, 96],
];

const skies = require("./skies").reverse();

const OUT_DIR = "./public";

(async () => {
  console.log("Getting content");
  const content = skies.reduce(
    (acc, { datetime, hex, name }) => `${acc}
      <section>
        <div>
          <h3>${name}</h3>
          <p>#${hex.toUpperCase()}</p>
          <time datetime="${datetime}">
            ${moment(datetime).format("h:mm a on MMMM Do YYYY")}
          </time>
        </div>
        <div class="color" style="background-color: #${hex}"></div>
      </section>`,
    ""
  );

  console.log("Getting styles");
  const styles = (await readFile("styles.css")).toString();

  console.log("Getting template");
  const template = await readFile("./index.html");

  console.log("Making directory");
  try {
    (await stat(OUT_DIR)).isDirectory();
  } catch (e) {
    await mkdir(OUT_DIR);
  }

  console.log("Composing header");
  const header = `<header>
  <h1>Probably Blue</h1>
  <h2>The sky has changed colors ${skies.length} times</h2>
</header>`;

  console.log("Creating favicons");
  const faviconColors = skies.slice(0, 9).map(({ hex }) => hex);
  const faviconHash = faviconColors.reduce((a, c) => `${a}${c}`, "");

  FAVICON_DIMENSIONS.forEach(async ([width, height]) => {
    const canvas = createCanvas(width, height);
    const context = canvas.getContext("2d");
    faviconColors.forEach((hex, i) => {
      context.fillStyle = `#${hex}`;
      const x = (i % 3) * (width / 3);
      const y = Math.floor(i / 3) * (height / 3);
      context.fillRect(x, y, x + width / 3, y + height / 3);
    });
    const buffer = canvas.toBuffer("image/png");
    await writeFile(
      `./public/favicon-${width}x${height}-${faviconHash}.png`,
      buffer
    );
  });

  const favicons = FAVICON_DIMENSIONS.reduce(
    (a, [width, height]) => `${a}
    <link
      rel="icon"
      type="image/png"
      href="https://probably.blue/favicon-${width}x${height}-${faviconHash}.png"
      sizes="${width}x${height}"
    />
    `,
    ""
  );

  console.log("Writing file");

  await writeFile(
    "./public/index.html",
    template
      .toString()
      .replace("<!-- STYLES -->", styles)
      .replace("<!-- FAVICONS -->", favicons)
      .replace("<!-- HEADER -->", header)
      .replace("<!-- CONTENT -->", content)
  );

  const TITLE = "probably.blue";
  const URL = "https://probably.blue";
  const DESCRIPTION = "The sky keeps changing colors";
  console.log("Writing RSS feed");
  const items = skies.reduce(
    (acc, { hex, name }) => `${acc}
<item>
  <title>${name}</title>
  <link>${URL}</link>
  <description>#${hex}</description>
</item>`,
    ""
  );
  await writeFile(
    `${OUT_DIR}/feed.xml`,
    `<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0">
<channel>
<title><!-- TITLE --></title>
<link><!-- URL --></link>
<description><!-- DESCRIPTION --></description>
<language>en-us</language>
<!-- ITEMS -->
</channel>
</rss>`
      .replace("<!-- TITLE -->", TITLE)
      .replace("<!-- URL -->", URL)
      .replace("<!-- DESCRIPTION -->", DESCRIPTION)
      .replace("<!-- ITEMS -->", items)
  );
})();
